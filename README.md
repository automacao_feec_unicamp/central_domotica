# Central Domótica

![Video](https://gitlab.com/automacao_feec_unicamp/central_domotica/wikis/blob/video.mp4)

Central de comunicações e controle domótico.

Informações: [wiki](https://gitlab.com/automacao_feec_unicamp/central_domotica/wikis/home)

* [Vídeo 1](https://www.youtube.com/watch?v=CdheCgQSsn8)
* [Vídeo 2](https://www.youtube.com/watch?v=2iCUksy8ENw)

## Autores

Juan Mejia Vallejo, Leonid Huancachoque Mamani
