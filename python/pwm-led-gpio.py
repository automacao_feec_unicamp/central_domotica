# Using PWM with RPi.GPIO

import RPi.GPIO as GPIO
import time
import sys
from pubnub import Pubnub
import serial

GPIO.setmode(GPIO.BCM)

PIN_PORTA = 13
PIN_GARAGE =18
SWITCH_DORMITORIO=21
SWITCH_SALA=20
SWITCH_PATIO=16

GPIO.setup(PIN_PORTA,GPIO.OUT)
GPIO.setup(SWITCH_SALA,GPIO.OUT)
GPIO.setup(SWITCH_DORMITORIO,GPIO.OUT)
GPIO.setup(PIN_GARAGE, GPIO.OUT)
GPIO.setup(SWITCH_PATIO, GPIO.OUT)


FREQ = 100 # frequency in Hz
pwm = GPIO.PWM(18, 100)
pwm .start(7.5)

# PubNub

pubnub = Pubnub(publish_key='demo', subscribe_key='demo')

channel = 'pi-house'
channel1 = 'pi-house1'
def callback(message):
	#print temp
	temp2=0.05
	#hum=0.07 #{1:0.1f}.format(h)
	#message = {'temperature': temp, 'humidity': hum}   
	#pubnub.publish(channel=channel, message=message, callback=callback, error=callback)
	#print(message)

def _callback1(m, channel):
	print(m)
	
	if m['item'] == 'light-living':
		dc = m['brightness'] * 10
		#living.ChangeDutyCycle(dc)

        
	elif m['item'] == 'door':
		print 'item' ,  m['openport'] 
		GPIO.output(PIN_PORTA,m['openport'] )
	
		
	elif m['item'] == 'garage':
		if (m['openga']==0):
			pwm.ChangeDutyCycle(7.5)
		else :
			pwm.ChangeDutyCycle(2.5) 
		print m['openga']
		
		print 'item' , m['openga']		
		
	elif m['item'] == 'switch_sala':
		print 'item' , m['open']
		GPIO.output(SWITCH_SALA,m['open'])	
		
	elif m['item'] == 'switch_dormi':
		print 'item' , m['trued']
		GPIO.output(SWITCH_DORMITORIO,m['trued'])	
		
	elif m['item'] == 'switch_patio':
		print 'item' , m['turn']
		GPIO.output(SWITCH_PATIO,m['turn'])	
	

def _error(m):
  print(m)

#data = _callback1


pubnub.subscribe(channels=channel1, callback=_callback1, error=_error)
ser = serial.Serial('/dev/ttyUSB0',9600)
s = [0,1]
try:
    while 1:
	#print 'retonro serial %s' % (ser.readline())	
	#input_pin_patio = GPIO.input(PIN_PATIO)
	#input_pin_sala = GPIO.input(PIN_SALA)
	#print('pin patio',input_pin_patio)
	#print('pin sala',input_pin_sala)
	#print ser.readline().translate(None,'\n\t\r')
	#print('data '+str(data))
	#ser.write(data)
		
	try:
		message=eval(ser.readline().translate(None,'\n\t\r'))
		print(message)	
		serial
		pubnub.publish(channel=channel, message=message, callback=callback, error=callback)
		pass
	except:
		print ('erro na leitura serial')
except KeyboardInterrupt:
    GPIO.cleanup()
    sys.exit(1)



